import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(private localNotifications: LocalNotifications, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public alertCtrl: AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      if(platform.is('android')) {
          statusBar.styleBlackOpaque();
        }
      else
      {
          statusBar.styleDefault();
      }
      splashScreen.hide();

      this.localNotifications.schedule({
        id: Math.floor(Math.random() * 100) + 1,
        title: 'The big survey - ' + Math.floor(Math.random() * 100) + 1,
        text: 'Single Local Notification',
        data: { mydata: ' mydata text' },
        attachments: ['https://picsum.photos/600/400/?random'],
        icon: 'https://picsum.photos/32/32?random',
        vibrate: true,
        trigger: { at: new Date(new Date().getTime() + 150 * 1000)}
      });


       setInterval(function () {
           // this.localNotifications.update({
           //     id: 10,
           //     title: "Meeting in 5 minutes! - " + Math.floor(Math.random() * 100) + 1,
           //     attachments: ['https://picsum.photos/600/400/?random'],
           //     icon: 'https://picsum.photos/32/32?random',
           // });

           this.localNotifications.schedule({
             id: Math.floor(Math.random() * 100) + 1,
             title: 'The setInterval - ' + Math.floor(Math.random() * 100) + 1,
             text: 'setInterval Local Notification',
             data: { mydata: ' mydata text' },
             attachments: ['https://picsum.photos/600/400/?random'],
             icon: 'https://picsum.photos/32/32?random',
             vibrate: true
           });

       }, 100000);


     });


  }
}
