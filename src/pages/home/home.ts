import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
// import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  actions?: any[];

  constructor( public navCtrl: NavController,
    platform:Platform,
    statusBar: StatusBar) {
    // localNotifications.on('yes', ()=>{
    //       // this.navCtrl.push(ListPage)
    //     })

      platform.ready().then((rdy) => {

       //  this.localNotifications.on('click', (notification)=>{
       //    let json = JSON.parse(notification.data);
       //    let alert = this.alertCtrl.create({
       //      title : notification.title,
       //      subTitle : json.mydata
       //    });
       //    alert.present();
       // });


       // this.localNotifications.schedule({
       //   id: 10,
       //   title: 'The big survey - ' + Math.floor(Math.random() * 100) + 1,
       //   text: 'Single LocalNotification',
       //   data: { mydata: ' mydata text' },
       //   attachments: ['https://picsum.photos/600/400/?random'],
       //   icon: 'https://picsum.photos/32/32?random',
       //   vibrate: true,
       // });
       //
       //
       //  setTimeout(function () {
       //      this.localNotification.update({
       //          id: 10,
       //          title: "Meeting in 5 minutes! - " + Math.floor(Math.random() * 100) + 1,
       //          attachments: ['https://picsum.photos/600/400/?random'],
       //          icon: 'https://picsum.photos/32/32?random',
       //      });
       //  }, 60000);


      });
  }


  // singlenotification(){
  //   this.localNotifications.schedule({
  //     id: 1,
  //     title: 'The big survey',
  //     text: 'Single ILocalNotification',
  //     // at: new Date(new Date().getTime() +5 * 3600),
  //     // data = { mydata:' mydata text'},
  //     attachments: ['https://picsum.photos/600/400/?random'],
  //     icon: 'https://picsum.photos/32/32?random',
  //     vibrate: true,
  //     actions: [
  //       { id: 'yes', title: 'Yes' },
  //       { id: 'no',  title: 'No' }
  //     ],
  //   });
  // }


}
